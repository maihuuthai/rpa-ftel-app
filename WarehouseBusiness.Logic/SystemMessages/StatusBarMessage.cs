﻿using Catel.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarehouseBusiness.Logic.SystemMessages
{
    public class StatusBarMessage : MessageBase<StatusBarMessage, StatusBarMessage.StatusMessage>
    {
        public StatusBarMessage() { }
        public class StatusMessage
        {
            /// <summary>
            /// Hien message tai status bar
            /// </summary>
            /// <param name="message"></param>
            /// <param name="seconds">default hien 10s</param>
            public StatusMessage(string message, int seconds = 10)
            {
                DurationSecond = seconds;
                Message = message;
            }
            public string Message { get; set; }
            /// <summary>
            /// hien message trong bao nhieu giay
            /// </summary>
            public int DurationSecond { get; set; }
        }
    }
}
