﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Logic.Models.Common;

namespace WarehouseBusiness.Logic.Constants
{
    public static class Options
    {
        public enum BrowserOptions
        {
            [Description("---Chọn---")]
            None = 0,
            [Description("Firefox")]
            Firefox = 1,
            [Description("Google Chrome")]
            Chrome = 2
            //[Description("Microsoft Edge")]
            //Edge = 3
        }
        public enum ServiceTypes
        {
            [Description("Truyền số liệu - MPLS")]
            MPLS = 11004,
            [Description("Internet - GIA NIX")]
            GIANIX = 11012,
            [Description("SD WAN_ILL")]
            SDWAN_ILL = 11019
        }
        public enum ServiceGroups
        {
            [Description("Kênh truyền")]
            KenhTruyen=11000
        }
        public static List<KeyValueModel> SpeedsNetwork = new List<KeyValueModel>()
        {
            new KeyValueModel(){Key=1,Value="Bình thường",HiddenValue="1"},
            new KeyValueModel(){Key=2,Value="Chậm",HiddenValue="1.5"},
            new KeyValueModel(){Key=3,Value="Không ổn định",HiddenValue="2"}
        };
    }
}
