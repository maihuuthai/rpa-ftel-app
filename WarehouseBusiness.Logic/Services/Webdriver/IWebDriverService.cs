﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Logic.Models.Common;

namespace WarehouseBusiness.Logic.Services.Webdriver
{
    public interface IWebDriverService
    {
        ErrorMessageModel<IWebDriver> GetInstanceDriver();
        void DestroySession();
    }
}
