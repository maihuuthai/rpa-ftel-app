﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Core.Extensions;
using WarehouseBusiness.Logic.Constants;
using WarehouseBusiness.Logic.Models.Common;
using WarehouseBusiness.Logic.Stores;

namespace WarehouseBusiness.Logic.Services.Webdriver
{

    public class WebDriverService : IWebDriverService
    {
        public void DestroySession()
        {
            try
            {
                if(SettingMemory.WebDriver.Driver != null)
                {                   
                    SettingMemory.WebDriver.Driver.Quit();
                }                
            }
            catch (ObjectDisposedException ex)
            {

            }            
        }

        public ErrorMessageModel<IWebDriver> GetInstanceDriver()
        {
            var browserOptionCurrent = SettingMemory.BrowserSetting.Kind;
            if (browserOptionCurrent <= 0)
            {
                return new ErrorMessageModel<IWebDriver>()
                {
                    Kind = KindError.Logic,
                    Message = "Chưa cài đặt trình duyệt chạy"
                };
            }
            DestroySession();
            //setup driver            
            try
            {
                if (SettingMemory.BrowserSetting.Kind == (int)Options.BrowserOptions.Chrome)
                {
                    var chromeOptions = new ChromeOptions()
                    {
                        PageLoadStrategy = PageLoadStrategy.Normal,
                        DebuggerAddress = "127.0.0.1:9222"
                    };
                    var policy = Policy
                                  .Handle<InvalidOperationException>()
                                  .WaitAndRetry(10, t => TimeSpan.FromSeconds(1));
                    policy.Execute(() =>
                    {
                        SettingMemory.WebDriver.Driver = new ChromeDriver(@".\Assets\chromedriver", chromeOptions);
                    });
                    return new ErrorMessageModel<IWebDriver>() { Data = SettingMemory.WebDriver.Driver };
                }
                else if (SettingMemory.BrowserSetting.Kind == (int)Options.BrowserOptions.Firefox)
                {
                    var firefoxOptions = new FirefoxOptions()
                    {
                        PageLoadStrategy = PageLoadStrategy.Normal
                    };
                    var driver = new FirefoxDriver(@".\Assets\firefoxdriver", firefoxOptions);
                    return new ErrorMessageModel<IWebDriver>() { Data = driver };
                }
                else
                {
                    return new ErrorMessageModel<IWebDriver>() { Kind = KindError.Logic, Message = "Không có trình duyệt tương thích" };
                }
            }
            catch (DriverServiceNotFoundException ex1)
            {
                return new ErrorMessageModel<IWebDriver>()
                {
                    Kind = KindError.Exception,
                    Message = $"{ex1.Message}",
                    Description = ex1.StackTrace
                };
            }
        }
    }
}
