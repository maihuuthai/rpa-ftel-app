﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Logic.Services.Common
{
    public interface IAuthService
    {
        Task LogOut();
        Task<bool> IsLoggedIn();
        Task<bool> LogIn(string username,string password);
    }
}
