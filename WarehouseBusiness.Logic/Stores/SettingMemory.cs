﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Logic.Stores
{
    public static class SettingMemory
    {
        public static class BrowserSetting
        {
            /// <summary>
            /// ten mien de truy cap test
            /// </summary>
            public static string Domain = "http://ftms-stag.fpt.net";
            /// <summary>
            /// loai trinh duyet su dung
            /// </summary>
            public static int Kind = 2;
        }
        public static class WebDriver
        {
            public static IWebDriver Driver;
        }
    }
}
