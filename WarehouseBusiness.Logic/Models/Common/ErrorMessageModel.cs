﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Logic.Models.Common
{
    public class ErrorMessageModel<TResult>:IDisposable
    {
        private bool disposedValue;
        public TResult Data { get; set; }
        public string Message { get; set; }
        public string Description { get; set; }
        public KindError Kind { get; set; } = KindError.None;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~ErrorMessageModel()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
    public enum KindError
    {
        None = 0,
        Logic = 1,
        Exception = 2,
        Library = 3
    }
}
