﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Logic.Models.Common
{
    public class LogsModel
    {
        public KindLog Kind { get; set; }
        public string KindName { get; set; }
        public string StepName { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string StringDateTime { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
    }
    public enum KindLog
    {
        [Description("Thông tin")]
        Info = 1,
        [Description("Lỗi")]
        Error = 2,
    }
}
