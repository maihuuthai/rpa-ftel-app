﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Logic.Models.Common
{
    public class KeyValueModel
    {
        public int Key { get; set; }
        public string Value { get; set; }
        public string HiddenValue { get; set; }
    }
}
