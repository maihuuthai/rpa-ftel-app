﻿using Catel.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Core.Infras
{
    public abstract class OwnerViewModelBase : ViewModelBase
    {
        protected OwnerViewModelBase() : base()
        {

        }
        public bool IsBusy { get; set; }
        protected override Task InitializeAsync()
        {
            ValidateViewModel(true);
            return base.InitializeAsync();
        }
    }
}
