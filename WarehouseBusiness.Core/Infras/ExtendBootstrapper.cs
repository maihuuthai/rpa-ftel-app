﻿using AutoMapper;
using Catel;
using Catel.IoC;
using Catel.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Core.Infras
{
    public sealed class ExtendBootstrapper
    {
        public static void Init(IServiceLocator container, string[] viewNamespaces,string[] viewModelNamespaces)
        {
            Argument.IsNotNull("config.Container", container);
            Argument.IsNotNull("config.ViewNamespaces", viewNamespaces);
            Argument.IsNotNull("config.ViewModelNamespaces", viewModelNamespaces);
            //ConfigureMapper();
            InitNamespaces(container, viewNamespaces,viewModelNamespaces);
        }

        private static void InitNamespaces(IServiceLocator container, string[] viewNamespaces, string[] viewModelNamespaces)
        {
            container.RegisterTypeIfNotYetRegistered<IViewLocator, ViewLocator>();
            container.RegisterTypeIfNotYetRegistered<IViewModelLocator, ViewModelLocator>();

            var viewLocator = container.ResolveType<IViewLocator>();
            viewLocator.NamingConventions.AddRange(viewNamespaces);

            var viewModelLocator = container.ResolveType<IViewModelLocator>();
            viewModelLocator.NamingConventions.AddRange(viewModelNamespaces);
        }

        private static void ConfigureMapper()
        {
            var profiles = new List<Profile>();
            var profileType = typeof(Profile);
            var assemblies = new List<Assembly>();
            //manually add

            //auto scan lib dlls
            assemblies.AddRange(AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => a.FullName.StartsWith("WarehouseBusiness")));
            foreach (var assembly in assemblies)
            {
                profiles.AddRange(
                    assembly.GetTypes()
                        // where the type implements the Profile type (or of the same type)
                        .Where(t => profileType.IsAssignableFrom(t) &&
                                    // get the default constructor for the profiles (one with no parameters)
                                    t.GetConstructor(Type.EmptyTypes) != null)
                        // Instantiate the profile.
                        .Select(Activator.CreateInstance)
                        .Cast<Profile>());
            }

            Mapper.Initialize(c => profiles.ForEach(c.AddProfile));
        }
    }
}
