﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Core.Exceptions
{
    public class MetadataException : Exception
    {
        public MetadataException(string message) : base(message)
        {

        }
    }
}
