﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Core.Exceptions
{
    public class ServerDisconnectException : Exception
    {
        public ServerDisconnectException() : base("Máy chủ từ chối kết nối, liên hệ quản trị hệ thống!")
        {

        }
    }
}
