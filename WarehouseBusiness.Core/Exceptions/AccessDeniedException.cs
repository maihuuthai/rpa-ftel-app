﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Core.Exceptions
{
    public class AccessDeniedException : Exception
    {
        public AccessDeniedException() : base("Phiên làm việc hiện tại đã kết thúc!")
        {

        }
    }
}
