﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseBusiness.Core.Constants
{
    public static class AppearanceRegions
    {
        public const string MainContent = "MainContent";
        public const string Navigation = "Navigation";
        public const string SubNavigation = "SubNavigation";
        public const string Menu = "MenuContent";
    }
}
