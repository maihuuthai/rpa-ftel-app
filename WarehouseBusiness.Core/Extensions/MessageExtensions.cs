﻿using Catel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarehouseBusiness.Core.Extensions
{
    public static class MessageExtensions
    {
        private static readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1, 1);//cung 1 thoi diem chi 1 thread duoc su dung
        private static bool _isBusy;

        /// <summary>
        /// show dialog confirm
        /// </summary>
        /// <param name="messageService"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static async Task<MessageResult> ShowCustomConfirmAsync(this IMessageService messageService, string message)
        {
            return await messageService.ShowAsync(message ?? string.Empty, "XÁC NHẬN", button: MessageButton.YesNo, icon: MessageImage.Question);
        }
        /// <summary>
        /// show dialog information
        /// </summary>
        /// <param name="messageService"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static async Task ShowCustomInfoAsync(this IMessageService messageService, string message)
        {
            if (_isBusy) return;
            await _semaphoreSlim.WaitAsync();
            _isBusy = true;
            await messageService.ShowAsync(message ?? string.Empty, "THÔNG TIN", MessageButton.OK, MessageImage.Information);
            _isBusy = false;
            _semaphoreSlim.Release();
        }
        /// <summary>
        /// show dialog warning
        /// </summary>
        /// <param name="messageService"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static async Task ShowCustomWarningAsync(this IMessageService messageService, string message)
        {
            if (_isBusy) return;
            await _semaphoreSlim.WaitAsync();
            _isBusy = true;
            await messageService.ShowAsync(message ?? string.Empty, "CẢNH BÁO", MessageButton.OK, MessageImage.Warning);
            _isBusy = false;
            _semaphoreSlim.Release();
        }
        /// <summary>
        /// show dialog error
        /// </summary>
        /// <param name="messageService"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static async Task ShowCustomErrorAsync(this IMessageService messageService, string message)
        {
            if (_isBusy) return;
            await _semaphoreSlim.WaitAsync();
            _isBusy = true;
            await messageService.ShowAsync(message ?? string.Empty, "CÓ LỖI XẢY RA", MessageButton.OK, MessageImage.Error);
            _isBusy = false;
            _semaphoreSlim.Release();
        }
    }
}
