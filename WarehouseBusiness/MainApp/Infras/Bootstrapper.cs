﻿using Catel;
using Catel.ExceptionHandling;
using Catel.IoC;
using Catel.Logging;
using Catel.Services;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Core.Constants;
using WarehouseBusiness.Core.Exceptions;
using WarehouseBusiness.Core.Extensions;
using WarehouseBusiness.Core.Infras;
using WarehouseBusiness.Logic.Services.Common;
using WarehouseBusiness.Views.Common;
using Catel.Extensions.Prism;

namespace WarehouseBusiness.Infras
{
    /// <summary>
    /// dang bi risk .dll Catel.Extensions.Prism neu get tu nuget (do dang bi loi nen phai add tay)
    /// </summary>
    public class Bootstrapper : BootstrapperBase<ShellView>
    {
        /// <summary>
        /// nap chuong trinh khoi tao view
        /// </summary>
        protected override void ConfigureContainer()
        {
            
            base.ConfigureContainer();
            //config mapper
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<SummarizeAutoMapper>();
            });
          
            LoggingSetting();
            ExceptionHandler();
            LocatorSetting();
            LanguageSetting();
            
        }

        #region methods
        /// <summary>
        /// configuration log listener on system application
        /// </summary>
        private void LoggingSetting()
        {
#if DEBUG
            LogManager.IsDebugEnabled = true;
#else
            LogManager.IsDebugEnabled=null;
#endif
            var fileLogListener = new FileLogListener();
            var logDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs");
            Directory.CreateDirectory(logDir);
            fileLogListener.FilePath = Path.Combine(logDir, DateTime.Today.ToString("yyyy-MM-dd") + ".log");

            LogManager.AddListener(fileLogListener);
        }
        /// <summary>
        /// xu ly loi global trong toan bo app
        /// </summary>
        private void ExceptionHandler()
        {
            var logger = LogManager.GetCurrentClassLogger();
            var exceptionService = base.Container.ResolveType<IExceptionService>();
            exceptionService.Register<AccessDeniedException>(async ex =>
            {
                base.Container.ResolveType<IPleaseWaitService>()?.Hide();
                await base.Container.ResolveType<IMessageService>()
                    .ShowCustomWarningAsync(ex.Message);
                await base.Container.ResolveType<IAuthService>().LogOut();
            }, null);
            exceptionService.Register<ServerDisconnectException>(async ex =>
            {
                logger.Error(ex);
                base.Container.ResolveType<IPleaseWaitService>()?.Hide();
                await base.Container.ResolveType<IMessageService>()
                    .ShowCustomErrorAsync(ex.Message);
            }, null);
            exceptionService.Register<MetadataException>(async ex =>
            {
                logger.Error(ex);
                base.Container.ResolveType<IPleaseWaitService>()?.Hide();
            }, null);
            exceptionService.Register<Exception>(async exception =>
            {
                logger.Error(exception);
                var pleaseWaitService = ServiceLocator.Default.ResolveType<IPleaseWaitService>();
                pleaseWaitService?.Hide();
#if DEBUG
                await Container.ResolveType<IMessageService>().ShowCustomErrorAsync(exception.ToString());
#else
                await Container.ResolveType<IMessageService>().ShowCustomErrorAsync("Đã có lỗi xảy ra. Vui lòng liên hệ quản trị");
#endif
                Environment.Exit(0);
            }, null);
        }
        /// <summary>
        /// dinh nghia map view - viewmodel
        /// </summary>
        private void LocatorSetting()
        {
            ExtendBootstrapper.Init(Container,
                new[]
                {
                    "WarehouseBusiness.Views.Common.[VM]View",
                    "WarehouseBusiness.Views.Dashboard.[VM]View",
                    "WarehouseBusiness.Views.Function.[VM]View",
                    "WarehouseBusiness.Views.Setting.[VM]View"
                },
                new[]
                {
                    "WarehouseBusiness.ViewModels.Common.[VW]ViewModel",
                    "WarehouseBusiness.ViewModels.Dashboard.[VW]ViewModel",
                    "WarehouseBusiness.ViewModels.Function.[VW]ViewModel",
                    "WarehouseBusiness.ViewModels.Setting.[VW]ViewModel"
                }
            );
        }
        /// <summary>
        /// setup language default on system application
        /// </summary>
        private void LanguageSetting()
        {
            var dependencyResolver = this.GetDependencyResolver();
            var languageService = dependencyResolver.Resolve<ILanguageService>();

            languageService.PreferredCulture = new CultureInfo("vi-VN");
            languageService.FallbackCulture = new CultureInfo("vi-VN");

            //var stringSource = new LanguageResourceSource("WarehouseBusiness.Logic", "WarehouseBusiness.Logic.Resources", "StringResource");
            //languageService.RegisterLanguageSource(stringSource);
        }

        #endregion
    }
}
