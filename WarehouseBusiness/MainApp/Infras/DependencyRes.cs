﻿using Catel.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Logic.Services.Common;
using WarehouseBusiness.Logic.Services.Webdriver;

namespace WarehouseBusiness.Infras
{
    public sealed class DependencyRes
    {
        public static void Register(IServiceLocator container)
        {
            //common
            container.RegisterType<IAuthService, AuthService>();
            //selenium webdriver
            container.RegisterType<IWebDriverService, WebDriverService>();
        }
    }
}
