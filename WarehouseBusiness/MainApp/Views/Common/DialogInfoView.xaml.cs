﻿using Catel.MVVM.Views;
using Catel.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WarehouseBusiness.Controls;

namespace WarehouseBusiness.Views.Common
{
    /// <summary>
    /// Interaction logic for DialogInfoView.xaml
    /// </summary>
    public partial class DialogInfoView : MetroDataWindow
    {
        private readonly IViewManager _viewManager;
        public DialogInfoView(IViewManager viewManager)
            : base(DataWindowMode.Custom, null, DataWindowDefaultButton.None, true,
                InfoBarMessageControlGenerationMode.None)
        {
            _viewManager = viewManager;
            InitializeComponent();            
        }
    }
}
