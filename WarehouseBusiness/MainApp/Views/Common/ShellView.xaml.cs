﻿using Catel.MVVM.Views;
using Catel.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WarehouseBusiness.Controls;

namespace WarehouseBusiness.Views.Common
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : MetroDataWindow
    {
        private readonly IViewManager _viewManager;
        public ShellView(IViewManager viewManager)
            : base(DataWindowMode.Custom, null, DataWindowDefaultButton.None, true,
                InfoBarMessageControlGenerationMode.None)
        {            
            _viewManager = viewManager;
            InitializeComponent();
        }
    }
}
