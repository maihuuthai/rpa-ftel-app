﻿using Catel.Collections;
using Catel.MVVM;
using Catel.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Core.Extensions;
using WarehouseBusiness.Core.Infras;
using WarehouseBusiness.Logic.Constants;
using WarehouseBusiness.Logic.Models.Common;
using WarehouseBusiness.Logic.Stores;
using WarehouseBusiness.Logic.SystemMessages;
using WarehouseBusiness.Properties;

namespace WarehouseBusiness.ViewModels.Common
{
    public class SettingViewModel : OwnerViewModelBase
    {
        private readonly IUICompositionService _uiCompositionService;
        private readonly IMessageService _messageService;
        public SettingViewModel(IUICompositionService uiCompositionService, IMessageService messageService)
        {
            _uiCompositionService = uiCompositionService;
            _messageService = messageService;
        }
        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();
            //init data
            ListBrowserOption = new FastObservableCollection<KeyValueModel>()
            {
                new KeyValueModel(){Key=(int)Options.BrowserOptions.None,Value=EnumHelper.GetDescription(Options.BrowserOptions.None)},
                new KeyValueModel(){Key=(int)Options.BrowserOptions.Firefox,Value=EnumHelper.GetDescription(Options.BrowserOptions.Firefox)},
                new KeyValueModel(){Key=(int)Options.BrowserOptions.Chrome,Value=EnumHelper.GetDescription(Options.BrowserOptions.Chrome)}                
            };
            SelectedBrowserOption = ListBrowserOption.FirstOrDefault(x => x.Key == SettingMemory.BrowserSetting.Kind);
            if (SelectedBrowserOption == null)
            {
                SelectedBrowserOption = ListBrowserOption.FirstOrDefault();
            }
            Domain = SettingMemory.BrowserSetting.Domain;
            PathApp = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
        }

        #region properties
        public FastObservableCollection<KeyValueModel> ListBrowserOption { get; set; }
        public KeyValueModel SelectedBrowserOption { get; set; }
        public string Domain { get; set; }
        public string PathApp { get; set; }
        #endregion

        #region commands
        #region save command

        private TaskCommand _saveCommand;
        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand SaveCommand => _saveCommand ?? (_saveCommand = new TaskCommand(Save, CanSave));
        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task Save()
        {
            if (base.IsBusy) return;
            SettingMemory.BrowserSetting.Domain = Domain;
            SettingMemory.BrowserSetting.Kind = SelectedBrowserOption?.Key ?? 0;
            StatusBarMessage.SendWith(new StatusBarMessage.StatusMessage(StringResources.StatusBar_SaveSuccess));
        }
        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanSave()
        {
            return true;
        }

        #endregion

        #region init session command

        private TaskCommand _initSessionCommand;
        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand InitSessionCommand => _initSessionCommand ?? (_initSessionCommand = new TaskCommand(InitSession, CanInitSession));
        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task InitSession()
        {
            if (base.IsBusy) return;
            try
            {
                Process proc = new Process();
                proc.StartInfo.FileName = PathApp;
                proc.StartInfo.Arguments = $"{SettingMemory.BrowserSetting.Domain} --new-window --remote-debugging-port=9222 --user-data-dir=C:\\Temp";
                var startOk = proc.Start();
            }
            catch (Exception e)
            {
                await _messageService.ShowErrorAsync("Sai đường dẫn ứng dụng.");
            }                       
        }
        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanInitSession()
        {
            return true;
        }

        #endregion
        #endregion
    }
}
