﻿using Catel.Services;
using Catel.Windows.Threading;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WarehouseBusiness.Core.Constants;
using WarehouseBusiness.Core.Infras;
using WarehouseBusiness.Logic.Services.Common;
using WarehouseBusiness.Logic.SystemMessages;
using WarehouseBusiness.Properties;
using WarehouseBusiness.ViewModels.Dashboard;
using WarehouseBusiness.Views.Common;

namespace WarehouseBusiness.ViewModels.Common
{
    public class ShellViewModel : OwnerViewModelBase
    {
        private readonly IUICompositionService _uiCompositionService;        
        public ShellViewModel(IUICompositionService uICompositionService)
        {
            _uiCompositionService = uICompositionService;
            _uiCompositionService.Activate<LoginViewModel>(AppearanceRegions.MainContent);            
        }
        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();
            Title = StringResources.Caption_MainView;
            SubcribeMessage();
        }
        protected override async Task CloseAsync()
        {
            UnsubcribeMessage();
            GC.Collect();
            await base.CloseAsync();
            Application.Current.Shutdown(0);
        }

        #region properties
        public bool IsLoggedIn { get; set; }
        public string CurrentStatus { get; set; }
        #endregion

        #region system messages
        private void SubcribeMessage()
        {            
            StatusBarMessage.Register(this, OnStatusBarUpdate);    
        }

        private void UnsubcribeMessage()
        {         
            StatusBarMessage.Unregister(this, OnStatusBarUpdate);    
        }

        private void OnStatusBarUpdate(StatusBarMessage msg)
        {
            DispatcherHelper.CurrentDispatcher.Invoke(async () =>
            {
                try
                {
                    var data = msg.Data;
                    CurrentStatus = data.Message;
                    await Task.Delay(data.DurationSecond * 1000);
                    //xoa status bar
                    if (CurrentStatus == data.Message) CurrentStatus = "";
                }
                catch (Exception)
                {
                    //ignore
                }
            });
        }
        #endregion
    }
}
