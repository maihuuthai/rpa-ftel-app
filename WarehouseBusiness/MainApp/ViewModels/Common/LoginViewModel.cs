﻿using Catel.IoC;
using Catel.MVVM;
using Catel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Core.Constants;
using WarehouseBusiness.Core.Infras;
using WarehouseBusiness.Logic.Services.Common;
using WarehouseBusiness.Logic.SystemMessages;
using WarehouseBusiness.Properties;
using WarehouseBusiness.ViewModels.Dashboard;

namespace WarehouseBusiness.ViewModels.Common
{
    public class LoginViewModel : OwnerViewModelBase
    {
        private readonly IUICompositionService _uiCompositionService;
        private readonly IUIVisualizerService _visualizerService;
        private readonly IAuthService _authService;
        private readonly IMessageService _messageService;
        public LoginViewModel(IUICompositionService uICompositionService, IUIVisualizerService visualizerService, IAuthService authService, IMessageService messageService)
        {
            _uiCompositionService = uICompositionService;
            _visualizerService = visualizerService;
            _authService = authService;
            _messageService = messageService;
        }
        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();
            Title = StringResources.Caption_Login;

            UserName = "thaimh";
            Password = "thaimh-pass";
        }

        #region properties
        public string UserName { get; set; }
        public string Password { get; set; }
        #endregion

        #region commands

        #region Login command

        private TaskCommand _loginCommand;

        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand LoginCommand => _loginCommand ?? (_loginCommand = new TaskCommand(Login, CanLogin));


        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task Login()
        {
            var isLoggedIn = await _authService.LogIn(UserName, Password);
            if (!isLoggedIn)
            {
                //show dialog popup
                //await _visualizerService.ShowDialogAsync<DialogInfoViewModel>();
                _ = await _messageService.ShowErrorAsync(StringResources.Dialog_LoginFail, StringResources.Caption_Login);
                return;
            }
            StatusBarMessage.SendWith(new StatusBarMessage.StatusMessage(StringResources.StatusBar_LoginSuccess));
            var _pleaseWaitService = ServiceLocator.Default.ResolveType<IPleaseWaitService>();
            _pleaseWaitService.Show(StringResources.Waiting_LoadingData);
            _uiCompositionService.Activate<MenuViewModel>(AppearanceRegions.Menu);
            _uiCompositionService.Activate<DashboardViewModel>(AppearanceRegions.MainContent);
            await Task.Delay(1000);
            _pleaseWaitService.Hide();
        }

        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanLogin()
        {
            return true;
        }

        #endregion

        #endregion
    }
}
