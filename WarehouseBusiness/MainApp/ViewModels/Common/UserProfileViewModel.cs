﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Core.Infras;

namespace WarehouseBusiness.ViewModels.Common
{
    public class UserProfileViewModel:OwnerViewModelBase
    {
        public UserProfileViewModel()
        {

        }

        protected override Task InitializeAsync()
        {
            Title = "USER PROFILE";
            return base.InitializeAsync();
        }
    }
}
