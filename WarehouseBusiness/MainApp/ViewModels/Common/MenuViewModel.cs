﻿using Catel.MVVM;
using Catel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Core.Constants;
using WarehouseBusiness.Core.Infras;
using WarehouseBusiness.Properties;
using WarehouseBusiness.ViewModels.Function;

namespace WarehouseBusiness.ViewModels.Common
{
    public class MenuViewModel : OwnerViewModelBase
    {
        private readonly IUICompositionService _uiCompositionService;

        public MenuViewModel(IUICompositionService uiCompositionService)
        {
            _uiCompositionService = uiCompositionService;
        }

        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();
        }

        #region commands

        #region function1 command

        private TaskCommand _function1Command;

        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand Function1Command => _function1Command ?? (_function1Command = new TaskCommand(Function1, CanFunction1));


        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task Function1()
        {
            _uiCompositionService.Activate<Function1ViewModel>(AppearanceRegions.MainContent);
        }

        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanFunction1()
        {
            return true;
        }

        #endregion

        #region function2 command

        private TaskCommand _function2Command;

        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand Function2Command => _function2Command ?? (_function2Command = new TaskCommand(Function2, CanFunction2));


        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task Function2()
        {
            _uiCompositionService.Activate<Function2ViewModel>(AppearanceRegions.MainContent);
        }

        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanFunction2()
        {
            return true;
        }

        #endregion

        #region setting command

        private TaskCommand _settingCommand;

        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand SettingCommand => _settingCommand ?? (_settingCommand = new TaskCommand(SettingPage, CanSettingPage));


        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task SettingPage()
        {
            _uiCompositionService.Activate<SettingViewModel>(AppearanceRegions.MainContent);
        }

        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanSettingPage()
        {
            return true;
        }

        #endregion

        #endregion
    }
}
