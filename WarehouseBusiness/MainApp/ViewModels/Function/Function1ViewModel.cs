﻿using Catel.Collections;
using Catel.MVVM;
using Catel.Services;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WarehouseBusiness.Core.Extensions;
using WarehouseBusiness.Core.Infras;
using WarehouseBusiness.Logic.Constants;
using WarehouseBusiness.Logic.Models.Common;
using WarehouseBusiness.Logic.Services.Webdriver;
using WarehouseBusiness.Logic.Stores;

namespace WarehouseBusiness.ViewModels.Function
{
    public class Function1ViewModel : OwnerViewModelBase
    {
        private readonly IWebDriverService _webDriverService;
        private readonly IMessageService _messageService;
        public Function1ViewModel(IWebDriverService webDriverService, IMessageService messageService)
        {
            _webDriverService = webDriverService;
            _messageService = messageService;
        }
        protected override Task InitializeAsync()
        {
            ListServiceType = new FastObservableCollection<KeyValueModel>()
            {
                new KeyValueModel(){Key=(int)Options.ServiceTypes.MPLS,Value=EnumHelper.GetDescription(Options.ServiceTypes.MPLS)},
                new KeyValueModel(){Key=(int)Options.ServiceTypes.GIANIX,Value=EnumHelper.GetDescription(Options.ServiceTypes.GIANIX)},
                new KeyValueModel(){Key=(int)Options.ServiceTypes.SDWAN_ILL,Value=EnumHelper.GetDescription(Options.ServiceTypes.SDWAN_ILL)}
            };
            SelectedServiceType = ListServiceType.FirstOrDefault();
            CusTax = "312274228";
            SaleName = "thaimh";
            Approver = "taipm";
            NameOpp = "cohoi_1";

            return base.InitializeAsync();
        }

        #region properties
        public FastObservableCollection<LogsModel> ListLog { get; set; } = new FastObservableCollection<LogsModel>();
        public string BrowserName { get; set; }

        public string NameOpp { get; set; }
        public FastObservableCollection<KeyValueModel> ListServiceType { get; set; }
        public KeyValueModel SelectedServiceType { get; set; }
        public string CusTax { get; set; }
        public string SaleName { get; set; }
        public string Approver { get; set; }
        #endregion

        #region commands
        #region run command

        private TaskCommand _runCommand;

        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand RunCommand => _runCommand ?? (_runCommand = new TaskCommand(Run, CanRun));


        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task Run()
        {
            var driver = _webDriverService.GetInstanceDriver();
            if (driver.Kind != (int)KindError.None)
            {
                await _messageService.ShowErrorAsync(driver.Message);
                return;
            }
            if (driver.Data == null)
            {
                await _messageService.ShowErrorAsync(driver.Message);
                return;
            }
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = "Khởi tạo phiên làm việc thành công!",
                Description = SettingMemory.WebDriver.Driver.Url
            });
            SettingMemory.WebDriver.Driver.Navigate().GoToUrl($"{SettingMemory.BrowserSetting.Domain}/bms/OppsMagt/Create");
            var checkLoginBms = SettingMemory.WebDriver.Driver.Manage().Cookies.AllCookies;
            if (!checkLoginBms.Any(x => x.Name == "FTIBMS-Auth"))
            {
                await _messageService.ShowErrorAsync("Bạn chưa đăng nhập trước khi chạy.");
                return;
            }

            //properties            
            string description = $"{NameOpp} - test - test - test";
            string serviceGroup = "Kênh truyền";
            string serviceType = SelectedServiceType?.Value ?? "";
            int reveneu = 90000000;
            string endDate = DateTime.Now.AddDays(3).ToString("dd/MM/yyyy");
            string cusTax = CusTax;
            string empAM = SaleName;
            string empApprove = Approver;
            //tab step1
            var inputNameOpp = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(3)).Until(drv => drv.FindElement(By.Id("txtNameOpp")));
            inputNameOpp.SendKeys(NameOpp);
            var selectServiceGroup = SettingMemory.WebDriver.Driver.FindElement(By.Name("serviceGroup_input"));
            selectServiceGroup.SendKeys(serviceGroup);
            new Actions(SettingMemory.WebDriver.Driver).Pause(TimeSpan.FromMilliseconds(1000)).SendKeys(Keys.Enter).Perform();
            await Task.Delay(250);
            var selectServiceType = SettingMemory.WebDriver.Driver.FindElement(By.Name("serviceType_input"));
            selectServiceType.SendKeys(serviceType);
            new Actions(SettingMemory.WebDriver.Driver).Pause(TimeSpan.FromMilliseconds(1000)).SendKeys(Keys.Enter).Perform();
            var inputDescription = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(3)).Until(drv => drv.FindElement(By.Id("txtDescription")));
            inputDescription.SendKeys(description);
            var inputRevenue = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(3)).Until(drv => drv.FindElement(By.Id("txtExpectedRevenue")));
            inputRevenue.SendKeys(reveneu.ToString());
            var inputEndDate = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(3)).Until(drv => drv.FindElement(By.Id("txtEndDate")));
            inputEndDate.SendKeys(endDate);
            var buttonTao = SettingMemory.WebDriver.Driver.FindElement(By.Id("btnInsOpp_Tao"));
            buttonTao.Click();
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = $"{NameOpp} - hoàn thành bước 1"
            });
            //tab step2
            await Task.Delay(500);
            var inputTax = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(3)).Until(drv => drv.FindElement(By.Id("customerSearchInput")));
            inputTax.SendKeys(cusTax);
            var buttonSearch = SettingMemory.WebDriver.Driver.FindElement(By.Id("btn-search"));
            buttonSearch.Click();
            new Actions(SettingMemory.WebDriver.Driver).Pause(TimeSpan.FromMilliseconds(1800)).SendKeys(Keys.ArrowDown).Perform();
            new Actions(SettingMemory.WebDriver.Driver).Pause(TimeSpan.FromMilliseconds(1000)).SendKeys(Keys.Enter).Perform();
            await Task.Delay(3000);
            var buttonTao1 = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(3)).Until(drv => drv.FindElement(By.Id("btnInsOpp_Tao")));
            buttonTao1.Click();
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = $"{NameOpp} - hoàn thành bước 2"
            });
            //tab step3
            var inputSale = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(3)).Until(drv => drv.FindElement(By.Name("saleName_input")));
            inputSale.SendKeys(empAM);
            new Actions(SettingMemory.WebDriver.Driver).Pause(TimeSpan.FromMilliseconds(800)).SendKeys(Keys.Enter).Perform();
            new Actions(SettingMemory.WebDriver.Driver).Pause(TimeSpan.FromMilliseconds(500)).SendKeys(Keys.Tab).SendKeys(empApprove).Pause(TimeSpan.FromMilliseconds(500)).SendKeys(Keys.Enter).Perform();
            var buttonTao2 = SettingMemory.WebDriver.Driver.FindElement(By.Id("btnInsOpp_Tao"));
            buttonTao1.Submit();
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = $"{NameOpp} - hoàn thành bước 3"
            });
            _webDriverService.DestroySession();
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = $"Đóng phiên hoàn tất"
            });
        }

        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanRun()
        {
            return true;
        }

        #endregion

        #region stop command

        private TaskCommand _stopCommand;

        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand StopCommand => _stopCommand ?? (_stopCommand = new TaskCommand(Stop, CanStop));


        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task Stop()
        {
            var confirmed = await _messageService.ShowCustomConfirmAsync("Lưu ý: Hành động này sẽ đóng trạng thái đang tương tác");
            if (confirmed == MessageResult.Yes)
            {
                ListLog.Add(new LogsModel()
                {
                    Kind = KindLog.Info,
                    KindName = EnumHelper.GetDescription(KindLog.Info),
                    StepName = "Đóng phiên làm việc"
                });
                _webDriverService.DestroySession();
            }
        }

        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanStop()
        {
            return true;
        }

        #endregion
        #endregion

        #region methods

        #endregion
    }
}
