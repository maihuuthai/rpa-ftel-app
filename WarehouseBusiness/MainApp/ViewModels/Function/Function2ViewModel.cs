﻿using Catel.MVVM;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using WarehouseBusiness.Core.Infras;
using WarehouseBusiness.Logic.Models.Common;
using WarehouseBusiness.Logic.Stores;
using Catel.Services;
using WarehouseBusiness.Logic.Services.Webdriver;
using WarehouseBusiness.Core.Extensions;
using Catel.Collections;
using System.CodeDom.Compiler;

namespace WarehouseBusiness.ViewModels.Function
{
    public class Function2ViewModel : OwnerViewModelBase
    {
        private readonly IMessageService _messageService;
        private readonly IWebDriverService _webDriverService;

        public Function2ViewModel(IMessageService messageService, IWebDriverService webDriverService)
        {
            _messageService = messageService;
            _webDriverService = webDriverService;
        }

        protected override Task InitializeAsync()
        {
            Bandwidth = 1;
            GlobalBandwidth = 1;
            TotalRecords = 3;
            WorkName = $"work_{DateTime.Now.Ticks}";
            Street = "duong so 1";
            ContactName = "thaimh";
            ContactPhone = "0379660460";
            ContactEmail = "thaimh@fpt.com.vn";
            return base.InitializeAsync();
        }

        #region properties
        public FastObservableCollection<LogsModel> ListLog { get; set; } = new FastObservableCollection<LogsModel>();
        public string OppCode { get; set; }
        public string WorkCode { get; set; }
        public string WorkName { get; set; }
        public int Bandwidth { get; set; }
        public int GlobalBandwidth { get; set; }
        public string Street { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public int TotalRecords { get; set; }
        #endregion

        #region commands

        #region run command

        private TaskCommand _runCommand;

        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand RunCommand => _runCommand ?? (_runCommand = new TaskCommand(Run, CanRun));


        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task Run()
        {
            var driver = _webDriverService.GetInstanceDriver();
            if (driver.Kind != (int)KindError.None)
            {
                await _messageService.ShowErrorAsync(driver.Message);
                return;
            }
            if (driver.Data == null)
            {
                await _messageService.ShowErrorAsync(driver.Message);
                return;
            }
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = "Khởi tạo phiên làm việc thành công!",
                Description = SettingMemory.WebDriver.Driver.Url
            });
            if (OppCode.IsNullOrEmpty())
            {
                await _messageService.ShowErrorAsync("Cần nhập mã cơ hội để tiếp tục");
                return;
            }
            if (WorkName.IsNullOrEmpty())
            {
                await _messageService.ShowErrorAsync("Cần nhập tên công việc để tiếp tục");
                return;
            }
            if (WorkCode.IsNullOrEmpty())
            {
                await tao_cong_viec();
            }
            else
            {
                await cap_nhat_cong_viec();
            }

            //redirect to Leasedline
            await Task.Delay(4000);
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = "Nhập liệu trên ứng dụng Leasedline",
                Description = SettingMemory.WebDriver.Driver.Url
            });
            var getCookies = SettingMemory.WebDriver.Driver.Manage().Cookies.AllCookies;
            if (!getCookies.Any(x => x.Name == "LL.COOKIE"))
            {
                await _messageService.ShowErrorAsync("Cần chứng thực trên Leasedline để thao tác tiếp");
                return;
            }
            if (ContactName.IsNullOrEmpty() || ContactPhone.IsNullOrEmpty() || Street.IsNullOrEmpty())
            {
                await _messageService.ShowErrorAsync("Nhập đủ thông tin trước khi chạy tự động");
                return;
            }
            //input head
            //input
            var selectRegionHead = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(5)).Until(drv => drv.FindElement(By.XPath("//*[@aria-labelledby='request_info_ddl_region_label']")));
            var choiceRandom = new Random();
            for (int i = 0; i < choiceRandom.Next(1, 2); i++)
            {
                await Task.Delay(100);
                selectRegionHead.SendKeys(Keys.ArrowDown);
            }
            for (int i = 1; i <= TotalRecords; i++)
            {
                await tao_dia_chi();
                var buttonAddNew = SettingMemory.WebDriver.Driver.FindElement(By.Id("create_survey_btn_add_deployment_address"));
                IJavaScriptExecutor javascriptExecutor = (IJavaScriptExecutor)SettingMemory.WebDriver.Driver;
                javascriptExecutor.ExecuteScript("arguments[0].click();", buttonAddNew);
                await Task.Delay(100);
            }
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = $"Đã thêm tổng cộng {TotalRecords} địa chỉ"
            });
            var buttonCreateRequest = SettingMemory.WebDriver.Driver.FindElement(By.Id("create_survey_btn_create_request_survey"));
            IJavaScriptExecutor javascriptExecutor1 = (IJavaScriptExecutor)SettingMemory.WebDriver.Driver;
            javascriptExecutor1.ExecuteScript("arguments[0].click();", buttonCreateRequest);
            new Actions(SettingMemory.WebDriver.Driver)
                .Pause(TimeSpan.FromMilliseconds(1000))
                .SendKeys(Keys.Enter)
                .Perform();
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = $"Quy trình tạo địa chỉ hoàn tất"
            });
        }
        private async Task cap_nhat_cong_viec()
        {
            SettingMemory.WebDriver.Driver.Navigate().GoToUrl($"{SettingMemory.BrowserSetting.Domain}/ll/bms/create/{WorkCode}");
            var getCookies = SettingMemory.WebDriver.Driver.Manage().Cookies.AllCookies;
            if (!getCookies.Any(x => x.Name == "LL.COOKIE"))
            {
                await _messageService.ShowErrorAsync("Cần chứng thực trên Leasedline để thao tác tiếp");
                return;
            }
        }
        private async Task tao_cong_viec()
        {
            SettingMemory.WebDriver.Driver.Navigate().GoToUrl($"{SettingMemory.BrowserSetting.Domain}/bms/Workv2/InsertWorkScreen?opportunityId={OppCode}");
            var checkLoginBms = SettingMemory.WebDriver.Driver.Manage().Cookies.AllCookies;
            if (!checkLoginBms.Any(x => x.Name == "FTIBMS-Auth"))
            {
                await _messageService.ShowErrorAsync("Bạn chưa đăng nhập trước khi chạy.");
                return;
            }
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = "Tạo công việc loại khảo sát",
                Description = SettingMemory.WebDriver.Driver.Url
            });
            var description = $"{OppCode} - {WorkName} - test";
            var inputWorkName = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(5)).Until(drv => drv.FindElement(By.Id("nameInsertWork")));
            inputWorkName.SendKeys(WorkName);
            new Actions(SettingMemory.WebDriver.Driver).Pause(TimeSpan.FromMilliseconds(800)).SendKeys(Keys.Tab).SendKeys(Keys.Tab).Pause(TimeSpan.FromMilliseconds(1000)).SendKeys(Keys.ArrowDown).SendKeys(Keys.ArrowDown).Perform();
            var inputDescription = SettingMemory.WebDriver.Driver.FindElement(By.Id("descriptionInsertWork"));
            inputDescription.SendKeys(description);
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = "Chuyển qua ứng dụng Leasedline",
                Description = SettingMemory.WebDriver.Driver.Url
            });
            await Task.Delay(500);
            var buttonNext = SettingMemory.WebDriver.Driver.FindElement(By.Id("btn-next-step"));
            buttonNext.Click();
        }
        private async Task tao_dia_chi()
        {
            var choiceRandom = new Random();
            var selectCable = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(5)).Until(drv => drv.FindElement(By.XPath("//*[@aria-labelledby='service_info_ddl_cable_label']")));
            for (int i = 0; i < choiceRandom.Next(1, 5); i++)
            {
                await Task.Delay(100);
                selectCable.SendKeys(Keys.ArrowDown);
            }
            var selectPort = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(5)).Until(drv => drv.FindElement(By.XPath("//*[@aria-labelledby='service_info_ddl_port_interface_label']")));
            for (int i = 0; i < choiceRandom.Next(1, 5); i++)
            {
                await Task.Delay(100);
                selectPort.SendKeys(Keys.ArrowDown);
            }
            var inputBandwidth = SettingMemory.WebDriver.Driver.FindElement(By.Id("service_info_input_bandwidth"));
            inputBandwidth.Clear();
            inputBandwidth.SendKeys(Bandwidth.ToString());
            try
            {
                var checkGlobalBandwidth = SettingMemory.WebDriver.Driver.FindElement(By.Id("service_info_input_international_bandwidth"));
                checkGlobalBandwidth.Clear();
                checkGlobalBandwidth.SendKeys(GlobalBandwidth.ToString());
            }
            catch (NoSuchElementException ex)
            {

            }
            catch (ElementNotInteractableException ex1)
            {

            }
            var selectRegion = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(5)).Until(drv => drv.FindElement(By.XPath("//*[@aria-labelledby='deployment_address_ddl_region_label']")));
            for (int i = 0; i < choiceRandom.Next(1, 3); i++)
            {
                await Task.Delay(100);
                selectRegion.SendKeys(Keys.ArrowDown);
            }
            await Task.Delay(1500);
            var selectBranch = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(5)).Until(drv => drv.FindElement(By.XPath("//*[@aria-labelledby='deployment_address_ddl_branch_label']")));
            for (int i = 0; i < choiceRandom.Next(2, 15); i++)
            {
                await Task.Delay(100);
                selectBranch.SendKeys(Keys.ArrowDown);
            }
            await Task.Delay(1000);
            var selectLocation = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(5)).Until(drv => drv.FindElement(By.XPath("//*[@aria-labelledby='deployment_address_ddl_location_label']")));
            selectLocation.SendKeys(Keys.ArrowDown);
            await Task.Delay(800);
            var selectDistrict = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(5)).Until(drv => drv.FindElement(By.XPath("//*[@aria-labelledby='deployment_address_ddl_district_label']")));
            for (int i = 0; i < choiceRandom.Next(1, 5); i++)
            {
                await Task.Delay(100);
                selectDistrict.SendKeys(Keys.ArrowDown);
            }
            await Task.Delay(1100);
            var selectWard = new WebDriverWait(SettingMemory.WebDriver.Driver, TimeSpan.FromSeconds(5)).Until(drv => drv.FindElement(By.XPath("//*[@aria-labelledby='deployment_address_ddl_ward_label']")));
            for (int i = 0; i < choiceRandom.Next(1, 5); i++)
            {
                await Task.Delay(100);
                selectWard.SendKeys(Keys.ArrowDown);
            }
            var inputStreet = SettingMemory.WebDriver.Driver.FindElement(By.Id("deployment_address_input_street"));
            inputStreet.SendKeys(Street);
            var inputContactName = SettingMemory.WebDriver.Driver.FindElement(By.Id("deployment_address_input_contact_name"));
            inputContactName.SendKeys(ContactName);
            var inputContactPhone = SettingMemory.WebDriver.Driver.FindElement(By.Id("deployment_address_input_contact_phone"));
            inputContactPhone.SendKeys(ContactPhone);
            var inputContactEmail = SettingMemory.WebDriver.Driver.FindElement(By.Id("deployment_address_input_contact_email"));
            inputContactEmail.Clear();
            inputContactEmail.SendKeys(ContactEmail);
            await Task.Delay(200);
            var inputAddress = SettingMemory.WebDriver.Driver.FindElement(By.Id("deployment_address_input_address"));
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = $"Thêm địa chỉ",
                Description = inputAddress.Text
            });
            var buttonCreateDraft = SettingMemory.WebDriver.Driver.FindElement(By.Id("deployment_address_btn_save"));
            IJavaScriptExecutor javascriptExecutor = (IJavaScriptExecutor)SettingMemory.WebDriver.Driver;
            javascriptExecutor.ExecuteScript("arguments[0].click();", buttonCreateDraft);

            _webDriverService.DestroySession();
            ListLog.Add(new LogsModel()
            {
                Kind = KindLog.Info,
                KindName = EnumHelper.GetDescription(KindLog.Info),
                StepName = $"Đóng phiên hoàn tất"
            });
        }

        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanRun()
        {
            return true;
        }

        #endregion

        #region stop command

        private TaskCommand _stopCommand;

        /// <summary>
        /// Gets the Login command.
        /// </summary>
        public TaskCommand StopCommand => _stopCommand ?? (_stopCommand = new TaskCommand(Stop, CanStop));


        /// <summary>
        /// Method to invoke when the Login command is executed.
        /// </summary>
        private async Task Stop()
        {
            var confirmed = await _messageService.ShowCustomConfirmAsync("Lưu ý: Hành động này sẽ đóng trạng thái đang tương tác");
            if (confirmed == MessageResult.Yes)
            {
                ListLog.Add(new LogsModel()
                {
                    Kind = KindLog.Info,
                    KindName = EnumHelper.GetDescription(KindLog.Info),
                    StepName = "Đóng phiên làm việc"
                });
                _webDriverService.DestroySession();
            }
        }

        /// <summary>
        /// Method to check whether the Login command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; otherwise <c>false</c></returns>
        private bool CanStop()
        {
            return true;
        }

        #endregion

        #endregion
    }
}
