﻿using Catel.ExceptionHandling;
using Catel.IoC;
using Catel.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WarehouseBusiness.Infras;
using WarehouseBusiness.Logic.Services.Common;

namespace WarehouseBusiness
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            //SplashScreen splash = new SplashScreen(@"Resources\Images\splash.png");
            //splash.Show(false, true);
            base.OnStartup(e);
            DependencyRes.Register(this.GetServiceLocator());
            new Bootstrapper().Run();
            //DependencyRes.Register(ServiceLocator.Default);

            //splash.Close(new TimeSpan(1));
            Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

        }
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            var pleaseWaitService = this.GetDependencyResolver().Resolve<IPleaseWaitService>();
            pleaseWaitService.Hide();

            var exceptionService = ServiceLocator.Default.ResolveType<IExceptionService>();
            exceptionService.HandleException(e.Exception);

            e.Handled = true;
        }
    }
}
